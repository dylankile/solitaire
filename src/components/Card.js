import React from 'react'

class Card extends React.Component {
	constructor(props) {
		super(props) 
		this.state = {
			value: props.content.value,
			suit: props.content.suit
		}
	}
	render() {
		// placeholders for suit display
		const suitMapping = {"spade": "S", 
							 "diamond": "D", 
							 "club": "C", 
							 "heart": "H"}
		const card = this.state
		return (
			<div className="card">
				<div className="card-value">{ card.value }</div>
				<div className="card-suit">{ suitMapping[card.suit] }</div>
			</div>
		)
	}
}

export default Card